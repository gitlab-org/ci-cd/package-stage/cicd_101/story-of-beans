import React from 'react'

const Banner = props => (
  <section id="banner" className="major">
    <div className="inner">
      <header className="major">
        <h1>Hi, my name is Beans</h1>
      </header>
      <div className="content">
        <p>
          I'm a cantankerous Lhasa Apso,
          <br />
          I live in Los Angeles, California.
        </p>
        <ul className="actions">
          <li>
            <a href="#one" name="Read my story" className="button next scrolly">
              Read my story.
            </a>
          </li>
        </ul>
      </div>
    </div>
  </section>
)

export default Banner
