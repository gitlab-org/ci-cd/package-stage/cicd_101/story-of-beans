# Background
This project has been created and maintained by @trizzi, to help learn more about GitLab and to demonstrate to others how to utilize some of the features in the Package stage at GitLab. Naturally, this will also include CI/CD features as well.

# Demo
Welcome to the GitLab Package stage demo! Today we are going to review a few key features and actions.

# Project Overview

- What is a project?
- How is it related to groups?
- How do I invite other contributors to my project?
- How do I remove other contributors from my project?

# Project Repository

- What files are we going to look at today?
  - [.gitlab-ci.yml](https://gitlab.com/gitlab-org/ci-cd/package-stage/cicd_101/story-of-beans/-/blob/master/.gitlab-ci.yml)
  - [Dockerfile](https://gitlab.com/gitlab-org/ci-cd/package-stage/cicd_101/story-of-beans/-/blob/master/Dockerfile)
  - [package.json](https://gitlab.com/gitlab-org/ci-cd/package-stage/cicd_101/story-of-beans/-/blob/master/package.json)
- Where can I find documentation about these files?
  - [Getting started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/)
  - [Building Docker images with GitLab CI/CD](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)
  - [GitLab NPM Registry](https://docs.gitlab.com/ee/user/packages/npm_registry/)
- How do I make changes to those files?
  - WebIDE
  - Work locally
- What is the WebIDE?
- What is an MR and how do I use them?
- How to work locally

# The Container Registry

- What is the Container Registry?
  - The registry is a storage and content delivery system, holding named Docker images, available in different tagged versions. Users interact with a registry by using docker push and pull commands.
- How do I log in?
  - Create a personal access token (scope set to `api`)
  - `docker login registry.gitlab.com`
  - Username = GitLab username
  - Password = GitLab personal access token
- How do I build an image?
  - Create a [Dockerfile](https://gitlab.com/gitlab-org/ci-cd/package-stage/cicd_101/story-of-beans/-/blob/master/Dockerfile)
  - `docker build -t registry.gitlab.com/gitlab-org/story-of-beans/image .`
- How do I publish an image?
  - `docker push registry.gitlab.com/gitlab-org/story-of-beans/image:latest`
- How can I view a list of my project and group images?
- How do I delete an image?
  - Via the [user interface](https://gitlab.com/gitlab-org/ci-cd/package-stage/cicd_101/story-of-beans/container_registry)
  - Via [CI/CD](https://gitlab.com/gitlab-org/ci-cd/package-stage/cicd_101/story-of-beans/-/blob/master/.gitlab-ci.yml)
  - Via the [Container Registry API](https://docs.gitlab.com/ee/api/container_registry.html#delete-a-registry-repository-tag)
  - Utilizing [Docker tag expiration policies](https://gitlab.com/gitlab-org/ci-cd/package-stage/cicd_101/story-of-beans/-/settings/ci_cd)
- Is there a way to build images faster?
  - Use [Docker Slim](https://github.com/docker-slim/docker-slim)
  - Cache images from DockerHub using the Dependency Proxy

# The Package Registry

- What is the Package Registry?
  - GitLab Packages allows organizations to utilize GitLab as a private repository for a variety of common package managers. Users are able to build and publish packages, which can be easily consumed as a dependency in downstream projects.
- Which package manager formats do you support?
  - We currently support npm, Maven, Conan, and NuGet.
- Which ones will you support?
  - For a list of supported and planned formats check [here](https://docs.gitlab.com/ee/user/packages/)
- How do I publish a package to my project?
  - Let's review an example using the  [NPM Registry](https://docs.gitlab.com/ee/user/packages/npm_registry/index.html)
- How do I install a package from another project?
  - Let's check out the [Package Registry UI](https://gitlab.com/gitlab-org/ci-cd/package-stage/cicd_101/story-of-beans/-/packages/105729)
  - Navigate to the project you'd like to install the package in
  - npm i @gitlab-org/beans
- Can I publish and install using pipelines?
  - Yes, check out the example in the [.gitlab-ci.yml](https://gitlab.com/gitlab-org/ci-cd/package-stage/cicd_101/story-of-beans/-/blob/master/.gitlab-ci.yml)
- How do I view a list of my packages?
  - Utilize the [Package Registry UI](https://gitlab.com/gitlab-org/ci-cd/package-stage/cicd_101/story-of-beans/-/packages/105729)
  - Utilize the [Package Registry API](https://docs.gitlab.com/ee/api/packages.html)

# How can I add a package or image to my Release?
